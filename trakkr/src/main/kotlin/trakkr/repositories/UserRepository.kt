package trakkr.repositories

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import trakkr.entities.User
/*
extend CrudRepository Interface to easily access the
basic db operations
*/

@Repository
interface UserRepository: CrudRepository<User, Long>