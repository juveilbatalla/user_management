package trakkr.entities

import javax.persistence.*


//Entity -> representation of a data in our table
// File name -> Pascal Case

@Entity(name = "User")
@Table(name = "users")
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null, // Long -> BIGINT

    @Column(
        nullable = false,
        updatable = true,
        name = "firstName"
    )
    var firstName: String,


    @Column(
        nullable = false,
        updatable = true,
        name = "lastName"
    )
    var lastName: String,


    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    var email: String,


    @Column(
        nullable = false,
        updatable = true,
        name = "isActive"
    )
    var isActive: Boolean = true,


    @Column(
        nullable = false,
        updatable = true,
        name = "userType"
    )
    var userType: String,
) {


}
// 1. Create entity
// 2. create SQL
// 3. create repository
// 4. create service
// 5. implement service
// 6. create handler
// Moving forward , repeat steps 4-6, revise step 1 and 2
// only if there are changes in the db structure